# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

> Please read [UPGRADING.md](UPGRADING.md) carrefully

:scroll: is a function \
:package: is a module \
:memo: is a script

## [1.0.1] - 2020-06-19

### Added

### Changed

-	100% code covergae

### Deprecated

### Removed

### Fixed

-	fixed :scroll: `Expand-SingleFileFromArchive` `-Force` parameter that could destroy data

### Security

## [1.0.0] - 2020-06-18

### Added

-	:scroll: `Get-ArchiveContent`
-	:scroll: `Expand-SingleFileFromArchive`
