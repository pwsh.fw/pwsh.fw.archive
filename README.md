# PwSh.Fw.Archive

Main repository is at https://gitlab.com/pwsh.fw/pwsh.fw.archive.

Helper functions to extend native powershell archive manipulation.

Microsoft Powershell already provide native `Compress-Archive` and `Expand-Archive` from the `Microsoft.PowerShell.Archive` official module.

This module extends these archive capabilities.
