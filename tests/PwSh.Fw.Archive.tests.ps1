# Invoke code coverage
# Invoke-Pester -Script ./PwSh.Fw.Archive.tests.ps1 -CodeCoverage ../PwSh.Fw.Archive/PwSh.Fw.Archive.psm1

$BASENAME = Split-Path -Path $PSCommandPath -Leaf
$ModuleName = $BASENAME -replace ".tests.ps1"

# load header
. $PSScriptRoot/header.inc.ps1

Import-Module -FullyQualifiedName Microsoft.PowerShell.Archive -Force -ErrorAction stop
Import-Module -FullyQualifiedName $ROOTDIR/$ModuleName/$ModuleName.psm1 -Force -ErrorAction stop

Describe "$ModuleName" {

	# clean things
	Remove-Item "$TEMP/tests.zip" -Force -ErrorAction SilentlyContinue
	Remove-Item $TEMP/tests -Recurse -Force -ErrorAction SilentlyContinue
	Remove-Item $TEMP/tests2 -Recurse -Force -ErrorAction SilentlyContinue
	# create a test archive
	Compress-Archive $PSScriptRoot -DestinationPath "$TEMP/tests.zip" -Force

	Context "List archive" {

		It "Get-ArchiveContent throw on archive not found" {
			{ Get-ArchiveContent -Path "$TEMP/noXarchive.zip" } | should -Throw
		}

		It "Get-ArchiveContent return a list of files from an archive" {
			$files = Get-ArchiveContent -Path "$TEMP/tests.zip"
			$files | should -Contain "tests${DS}PwSh.Fw.Archive.tests.ps1"
		}

		It "Get-ArchiveContent return a list of zip archive entries" {
			$files = Get-ArchiveContent -Path "$TEMP/tests.zip" -PassThru
			$files | should -not -BeNullOrEmpty
			$files[0].GetType().Name | Should -BeExactly ZipArchiveEntry
		}

	}

	Context "Expand archive" {

		It "Expand-SingleFileFromArchive throw on archive not found" {
			{ "$TEMP/noXarchive.zip" | Expand-SingleFileFromArchive -file "header.inc.ps1" } | should -Throw
		}

		It "Expand-SingleFileFromArchive return false on file not found #1" {
			"$TEMP/tests.zip" | Expand-SingleFileFromArchive -file "noXfile.ext" | should -BeFalse
		}

		It "Expand-SingleFileFromArchive return false on file not found #2" {
			"$TEMP/tests.zip" | Expand-SingleFileFromArchive -file "noXfile.ext" -PassThru | should -BeFalse
		}

		It "Expand-SingleFileFromArchive return false #1 (destination folder does not exist)" {
			$rc = "$TEMP/tests.zip" | Expand-SingleFileFromArchive -file "tests/header.inc.ps1"
			$rc | should -BeFalse
			Test-Path -Path "$TEMP/tests/header.inc.ps1" | Should -BeFalse
		}

		It "Expand-SingleFileFromArchive return true #1" {
			$rc = "$TEMP/tests.zip" | Expand-SingleFileFromArchive -file "tests/header.inc.ps1" -Force
			$rc | should -BeTrue
			Test-Path -Path "$TEMP/tests/header.inc.ps1" | Should -BeTrue
		}

		It "Expand-SingleFileFromArchive return false #2 (destination folder does not exist)" {
			$rc = "$TEMP/tests.zip" | Expand-SingleFileFromArchive -file "tests/header.inc.ps1" -DestinationPath /tmp/tests2
			$rc | should -BeFalse
			Test-Path -Path "$TEMP/tests2/header.inc.ps1" | Should -BeFalse
		}

		It "Expand-SingleFileFromArchive return true #2" {
			$rc = "$TEMP/tests.zip" | Expand-SingleFileFromArchive -file "tests/header.inc.ps1" -DestinationPath /tmp/tests2 -Force
			$rc | should -BeTrue
			Test-Path -Path "$TEMP/tests2/header.inc.ps1" | Should -BeTrue
		}

		It "Expand-SingleFileFromArchive return false #2 (destination file already exist)" {
			$rc = "$TEMP/tests.zip" | Expand-SingleFileFromArchive -file "tests/header.inc.ps1" -DestinationPath /tmp/tests2
			$rc | should -BeFalse
			Test-Path -Path "$TEMP/tests2/header.inc.ps1" | Should -BeTrue
		}

		It "Expand-SingleFileFromArchive return true #2 (ovewrite destination file)" {
			$rc = "$TEMP/tests.zip" | Expand-SingleFileFromArchive -file "tests/header.inc.ps1" -DestinationPath /tmp/tests2 -Force
			$rc | should -BeTrue
			Test-Path -Path "$TEMP/tests2/header.inc.ps1" | Should -BeTrue
		}

		It "Expand-SingleFileFromArchive return path" {
			$rc = "$TEMP/tests.zip" | Expand-SingleFileFromArchive -file "tests/pester.inc.ps1" -PassThru -Force
			$rc | should -BeExactly "$TEMP${DS}tests${DS}pester.inc.ps1"
			Test-Path -Path "$TEMP/tests/pester.inc.ps1" | Should -BeTrue
		}

		It "Expand-SingleFileFromArchive return false #1" {
			$rc = "$TEMP/tests.zip" | Expand-SingleFileFromArchive -file "tests/header.inc.ps1" -DestinationPath /tmp/tests2
			$rc | should -BeFalse
			Test-Path -Path "$TEMP/tests2/header.inc.ps1" | Should -BeTrue
		}

	}

	# clean things
	# Remove-Item "$TEMP/tests.zip" -Force

}